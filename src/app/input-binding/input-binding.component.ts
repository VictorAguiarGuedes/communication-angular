import { Component, OnInit, Input } from '@angular/core';
import { Client } from './client.model';

@Component({
  selector: 'app-input-binding',
  templateUrl: './input-binding.component.html',
  styleUrls: ['./input-binding.component.scss']
})
export class InputBindingComponent implements OnInit {
  
  @Input() name: string;
  @Input('othername') lastName: string;
  @Input() age: number;

  clients: Client[];

  constructor() {
    this.clients = [
      {id: 1, name: "Paulo", age:30},
      {id: 2, name: "Maria", age:20},
      {id: 3, name: "John", age:25},
    ];
   }

  ngOnInit() {
  }

}
